import zmq
from zmq import Context, Socket
from zmq.eventloop import ioloop
from zmq.eventloop.zmqstream import ZMQStream

ioloop.install()

from tornado.websocket import WebSocketHandler
from tornado.web import Application
from tornado.ioloop import IOLoop

ioloop = IOLoop.instance()


class ZMQPubSub(object):

    stream: ZMQStream
    socket: Socket
    context: Context

    def __init__(self, callback):
        self.callback = callback

    def connect(self):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.SUB)
        self.socket.connect('tcp://127.0.0.1:5560')
        self.stream = ZMQStream(self.socket)
        self.stream.on_recv(self.callback)

    def subscribe(self, channel_id):
        self.socket.setsockopt(zmq.SUBSCRIBE, channel_id)


class MyWebSocket(WebSocketHandler):

    def open(self):
        self.pubsub = ZMQPubSub(self.on_data)
        self.pubsub.connect()
        self.pubsub.subscribe("session_id")
        print('ws opened')

    def on_message(self, message):
        print(message)

    def on_close(self):
        print('ws closed')

    def on_data(self, data):
        print(data)


def main():
    application = Application([(r'/channel', MyWebSocket)])
    application.listen(10001)
    print('starting ws on port 10001')
    ioloop.start()


if __name__ == '__main__':
    main()
