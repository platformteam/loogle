import webbrowser
import pyinotify
import logging

URL = "http://www.google.com"
FILE = "/home/tkim/test"

class EventHandler(pyinotify.ProcessEvent):
    def __init__(self, *args, **kwargs):
        super(EventHandler, self).__init__(*args, **kwargs)
        self.file = open(FILE)
        self.position = 0
        self.print_lines()

    def process_IN_MODIFY(self, event):
        print('event received')
        self.print_lines()

    def print_lines(self):
        new_lines = self.file.read()
        last_n = new_lines.rfind('\n')
        if last_n >= 0:
            self.position += last_n + 1
            print(new_lines[:last_n])
        else:
            print('no line')
        self.file.seek(self.position)




handler = EventHandler()
wm = pyinotify.WatchManager()
notifier = pyinotify.Notifier(wm, handler, timeout=10)
wdd = wm.add_watch(FILE, pyinotify.IN_MODIFY, rec=True)
notifier.loop()
