#!/usr/bin/env python
import os
import sys
import time
from os.path import dirname, exists

from watchdog.events import FileSystemEventHandler
from watchdog.observers.polling import PollingObserver


class TailHandler(FileSystemEventHandler):

    def __init__(self, path):
        self.path = path
        self.file = open(path, 'r')
        self.pos = os.stat(path)[6]

    def close(self):
        self.file.close()

    def print_line(self):
        self.file.seek(self.pos)
        for block in iter(lambda: self.file.read(32), ''):
            print(block, end='')
        self.pos = self.file.tell()

    def on_modified(self, event):
        if event.is_directory or self.path != event.src_path:
            return
        self.print_line()


def tail_like(path):
    observer = PollingObserver()
    handler = TailHandler(path)
    observer.schedule(handler, dirname(path))
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    finally:
        handler.close()
    observer.join()


def main():
    # path = sys.argv[1]
    path = "/home/tkim/test"
    if not exists(path):
        print('{} is not found'.format(path))
        return
    tail_like(path)


if __name__ == '__main__':
    main()
