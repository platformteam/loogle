import zmq
import random
import sys
import time

"""
Exclusive Pair Pattern

Exclusive pair pattern implies and allows establishing one-tone sort of communication channels using the zmq/PAIR socket type.
"""


port = "5556"
context = zmq.Context()
socket = context.socket(zmq.PAIR)
socket.bind("tcp://*:%s" % port)

while True:
    socket.send_string("Server message to client3")
    msg = socket.recv().decode()
    print(msg)
    time.sleep(1)
