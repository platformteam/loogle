import webbrowser
import pyinotify
import logging

URL = "http://www.google.com"


class ModHandler(pyinotify.ProcessEvent):
    # evt has useful properties, including pathname
    def stop(self, event):
        if hasattr(event, 'pathname'):
            logger = logging.getLogger()
            logger.info("Config file was changed, reloading it...")
        webbrowser.open(URL)

    process_IN_DELETE = stop
    process_IN_CLOSE = stop
    process_default = stop


FILE = "/home/tkim/test"

handler = ModHandler()
wm = pyinotify.WatchManager()
notifier = pyinotify.Notifier(wm, handler)
wdd = wm.add_watch(FILE, pyinotify.IN_CLOSE_WRITE | pyinotify.IN_DELETE)
notifier.loop()
